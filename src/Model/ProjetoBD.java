/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import tccmetricas.Persistencia;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author Witally
 */
public class ProjetoBD {

    protected PreparedStatement ps;
    protected ResultSet rs;
    protected Projeto p;
    private ObservableList<Projeto> listaProjetos = FXCollections.observableArrayList();

    public ProjetoBD() {
        ps = null;
        rs = null;
        p = new Projeto();
    }

    public ProjetoBD(PreparedStatement ps, ResultSet rs, Projeto p) {
        this.ps = ps;
        this.rs = rs;
        this.p = p;
    }

    /*    public int buscar(int codigoProjeto) {
        PreparedStatement ps = null;
        ResultSet rs = null;

        int idProjeto = 0;
        try {

            ps = Persistencia.conexao().prepareStatement("select * from projetoss WHERE codigoProjeto=?");
            ps.setInt(1, codigoProjeto);
            rs = ps.executeQuery();
            while (rs.next()) {
                idProjeto = rs.getInt("idProjeto");
            }
            rs.close();
        } catch (SQLException e) {
            System.out.println("Erro: " + e);

        }
        return idProjeto;
    }*/
    public void gravarProjetoNoBD(Projeto p) {
        try {
            ps = Persistencia.conexao().prepareStatement("INSERT INTO projetos(nomeProjeto, dataProjeto, tipoProjeto, gerenteProjeto, descricaoProjeto) values (?,?,?,?,?)");
            //ps = Persistencia.conexao().prepareStatement("Insert into projetos (nomeProjeto) values (?)");
            ps.setString(1, p.getNomeProjeto());
            ps.setDate(2, p.getDataProjeto());
            ps.setString(3, p.getTipoProjeto());
            ps.setString(4, p.getGerenteProjeto());
            ps.setString(5, p.getDescricaoProjeto());
            ps.execute();
            ps.close();
        } catch (SQLException e) {
            System.out.println("Erro: " + e);
        }
    }

    /*public void gravarProjetoNoRelatorio0(){
        try {
            Relatorio r = new Relatorio();
            ps = Persistencia.conexao().prepareStatement("Insert into relatorios(nomeProjeto=?, codigoProjeto=?,codigoRelatorio=?) values (?,?,?)");
            //ps = Persistencia.conexao().prepareStatement("Insert into projetos (nomeProjeto) values (?)");
            ps.setString(1, p.getNomeProjeto());
            ps.setInt(2, p.getCodigoProjeto());
            ps.setInt(3, 0);
            //ps.setDate(2, p.getDataProjeto());
            //ps.setString(3, p.getDescricaoProjeto());
            ps.execute();
            ps.close();
        } catch (SQLException e) {
            System.out.println("Erro: " + e);
        }
    
    }*/
    public ObservableList<Projeto> listaProjetosCadastrados() {

        try {
            ps = Persistencia.conexao().prepareStatement("SELECT* FROM projetos");
            rs = ps.executeQuery();

            while (rs.next()) {
                Projeto pLocal = new Projeto();
                pLocal.setCodigoProjeto(rs.getInt("codigoProjeto"));
                pLocal.setNomeProjeto(rs.getString("nomeProjeto"));
                pLocal.setDataProjeto(rs.getDate("dataProjeto"));
                pLocal.setTipoProjeto(rs.getString("tipoProjeto"));
                pLocal.setGerenteProjeto(rs.getString("gerenteProjeto"));
                pLocal.setDescricaoProjeto(rs.getString("descricaoProjeto"));

                listaProjetos.add(pLocal);
            }

            rs.close();
        } catch (SQLException e) {
            System.out.println("Erro: " + e);
        }

        return listaProjetos;
    }

    public Projeto buscarProjetoCadastrado(int codigo) {

        try {
            ps = Persistencia.conexao().prepareStatement("SELECT * FROM projetos WHERE codigoProjeto=?");
            ps.setInt(1, codigo);
            rs = ps.executeQuery();
            while (rs.next()) {
                String nome = rs.getString("nomeProjeto");
                Date data = rs.getDate("dataProjeto");
                String tipo = rs.getString("tipoProjeto");
                String gerente = rs.getString("gerenteProjeto");
                String descricao = rs.getString("descricaoProjeto");

                p.setCodigoProjeto(codigo);
                p.setNomeProjeto(nome);
                p.setDataProjeto(data);
                p.setTipoProjeto(tipo);
                p.setGerenteProjeto(gerente);
                p.setDescricaoProjeto(descricao);

                return p;
            }
            rs.close();

        } catch (SQLException e) {
            System.out.println("Erro: " + e);
        }
        return p;
    }

    public void alterarProjetoNoBD(int codigo) {

        if (jaExiste(codigo)) {
            //p.preencher();
            try {
                ps = Persistencia.conexao().prepareStatement("UPDATE projetos SET nomeProjeto=?, dataProjeto=?, tipoProjeto=?, gerenteProjeto=?, descricaoProjeto=? WHERE codigoProjeto=?");
                ps.setString(1, p.getNomeProjeto());
                ps.setDate(2, p.getDataProjeto());
                ps.setString(3, p.getTipoProjeto());
                ps.setString(4, p.getGerenteProjeto());
                ps.setString(5, p.getDescricaoProjeto());
                ps.setInt(6, codigo);

                ps.execute();
                System.out.println("Alterado!");

            } catch (SQLException e) {
                System.out.println("Erro: " + e);
            }
        } else {
            System.out.println("Código não encontrado!");
        }
    }

    /*
    public void imprimirListaProjetosCadastradas() {

        try {
            ps = Persistencia.conexao().prepareStatement("select* from projetos");
            rs = ps.executeQuery();
            System.out.println("_________________________________");
            System.out.println("|ID\t|Nome\t\t|CPF\t\t|E-mail\t\t\t|Sexo\t|Telefone");
            while (rs.next()) {
                int id = rs.getInt("idProjeto");
                String nome = rs.getString("nome");
                String cpf = rs.getString("cpf");
                String email = rs.getString("email");
                String sexo = rs.getString("sexo");
                String telefone = rs.getString("telefone");

                System.out.println("|" + id + "\t|" + nome + "\t\t|" + cpf + "\t\t|" + email + "\t\t\t|" + sexo + "\t|" + telefone);
                System.out.println("");
            }
            rs.close();
        } catch (SQLException e) {
            System.out.println("Erro: " + e);
        }
    }
     */
    public boolean jaExiste(int codigo) {
        try {
            ps = Persistencia.conexao().prepareStatement("SELECT * FROM projetos WHERE codigoProjeto=?");
            ps.setInt(1, codigo);
            rs = ps.executeQuery();
            if (rs.wasNull()) {
                return false;
            }
            rs.close();

        } catch (SQLException e) {
            System.out.println("Comando errado, PARCEIRO!" + e);
        }
        return true;
    }

    /*
    public void alterarProjetoNoBD(int codigo) {

        if (jaExiste(codigo)) {
            //p.preencher();
            try {
                ps = Persistencia.conexao().prepareStatement("Update projetos set nome=?, cpf=?, email=?, sexo=?, telefone=?, rua=?, numero=?, complemento=?, bairro=?, cidade=?, uf=? where idProjeto=?");
                ps.setString(1, p.getNome());
                ps.setString(2, p.getCpf());
                ps.setString(3, p.getEmail());
                ps.setString(4, p.getSexo());
                ps.setString(5, p.getTelefone());
                ps.setInt(6, codigo);
                ps.setString(7, p.getRua());
                ps.setInt(8, p.getNumero());
                ps.setString(9, p.getComplemento());
                ps.setString(10, p.getBairro());
                ps.setString(11, p.getCidade());
                ps.setString(12, p.getUf());
                ps.execute();
                System.out.println("Alterado com sucesso!");

            } catch (SQLException e) {
                System.out.println("Erro: " + e);
            }
        } else {
            System.out.println("Código não encontrado!");
        }
    }
     */
    public void remover(int codigo) {
        //if (jaExiste(codigo)) {
        try {
            ps = Persistencia.conexao().prepareStatement("DELETE FROM projetos WHERE codigoProjeto=?");
            ps.setInt(1, codigo);
            ps.executeUpdate();
            System.out.println("Projeto excluído com sucesso!");

        } catch (SQLException e) {
            Logger.getLogger(Projeto.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    /*} else {
            System.out.println("Código não encontrado!");
        }
    }*/
    /**
     * @return the ps
     */
    public PreparedStatement getPs() {
        return ps;
    }

    /**
     * @param ps the ps to set
     */
    public void setPs(PreparedStatement ps) {
        this.ps = ps;
    }

    /**
     * @return the rs
     */
    public ResultSet getRs() {
        return rs;
    }

    /**
     * @param rs the rs to set
     */
    public void setRs(ResultSet rs) {
        this.rs = rs;
    }

    /**
     * @return the p
     */
    public Projeto getP() {
        return p;
    }

    /**
     * @param p the p to set
     */
    public void setP(Projeto p) {
        this.p = p;
    }
}
