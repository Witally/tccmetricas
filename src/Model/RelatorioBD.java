/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import tccmetricas.Persistencia;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author PauloAfonso
 */
public class RelatorioBD {

    protected PreparedStatement ps;
    protected ResultSet rs;
    protected Relatorio r;
    private ObservableList<Relatorio> listaRelatorios = FXCollections.observableArrayList();

    public RelatorioBD() {
        ps = null;
        rs = null;
        r = new Relatorio();
    }

    public RelatorioBD(PreparedStatement ps, ResultSet rs, Relatorio r) {
        this.ps = ps;
        this.rs = rs;
        this.r = r;
    }

    /*    public int buscar(int codigoRelatorio) {
        PreparedStatement ps = null;
        ResultSet rs = null;

        int idRelatorio = 0;
        try {

            ps = Persistencia.conexao().prepareStatement("select * from relatorioss WHERE codigoRelatorio=?");
            ps.setInt(1, codigoRelatorio);
            rs = ps.executeQuery();
            while (rs.next()) {
                idRelatorio = rs.getInt("idRelatorio");
            }
            rs.close();
        } catch (SQLException e) {
            System.out.println("Erro: " + e);

        }
        return idRelatorio;
    }*/
    public void gravarRelatorioNoBD(Relatorio r) {
        try {
            ps = Persistencia.conexao().prepareStatement("Insert into relatorios(nomeProjeto, dataRelatorio, nomeMetrica, horas, codigoProjeto) values (?,?,?, ?, ?)");
            //ps = Persistencia.conexao().prepareStatement("Insert into relatorios (nomeProjeto) values (?)");
            ps.setString(1, r.getNomeRelatorio());
            ps.setDate(2, r.getDataRelatorio());
            ps.setString(3, r.getNomeMetrica());
            ps.setInt(4, r.getHoras());
            ps.setInt(5, r.getCodigoProjeto());
            ps.execute();
            ps.close();
        } catch (SQLException e) {
            System.out.println("Erro: " + e);
        }
    }

    public ObservableList<Relatorio> listaRelatoriosCadastrados() {

        try {
            ps = Persistencia.conexao().prepareStatement("select* from relatorios");
            rs = ps.executeQuery();

            while (rs.next()) {
                Relatorio rLocal = new Relatorio();
                rLocal.setNomeRelatorio(rs.getString("nomeProjeto"));
                rLocal.setDataRelatorio(rs.getDate("dataRelatorio"));
                rLocal.setNomeMetrica(rs.getString("nomeMetrica"));
                rLocal.setHoras(rs.getInt("horas"));
                rLocal.setCodigoRelatorio(rs.getInt("codigoRelatorio"));
                rLocal.setCodigoProjeto(rs.getInt("codigoProjeto"));
                listaRelatorios.add(rLocal);
            }

            rs.close();
        } catch (SQLException e) {
            System.out.println("Erro: " + e);
        }

        return listaRelatorios;
    }

    public Relatorio buscarRelatorioCadastrado(int codigo) {

        try {
            ps = Persistencia.conexao().prepareStatement("select * from relatorios where codigoRelatorio=?");
            ps.setInt(1, codigo);
            rs = ps.executeQuery();
            while (rs.next()) {
                String nome = rs.getString("nomeProjeto");
                Date data = rs.getDate("dataRelatorio");
                String nomeMetrica = rs.getString("nomeMetrica");
                int horas = rs.getInt("horas");
                int codigoProjeto = rs.getInt("codigoProjeto");

                r.setCodigoRelatorio(codigo);
                r.setHoras(horas);
                r.setNomeRelatorio(nome);
                r.setDataRelatorio(data);
                r.setNomeMetrica(nomeMetrica);
                r.setCodigoProjeto(codigoProjeto);

                return r;
            }
            rs.close();
        } catch (SQLException e) {
            System.out.println("Erro: " + e);
        }
        return r;
    }

    public void alterarRelatorioNoBD(int codigo) {

        if (jaExiste(codigo)) {
            //r.preencher();
            try {
                ps = Persistencia.conexao().prepareStatement("Update relatorios set nomeProjeto=?, dataRelatorio=?, nomeMetrica=?, horas=?, codigoProjeto=? where codigoRelatorio=?");
                ps.setString(1, r.getNomeRelatorio());
                ps.setDate(2, r.getDataRelatorio());
                ps.setString(3, r.getNomeMetrica());
                ps.setInt(4, r.getHoras());
                ps.setInt(5, r.getCodigoProjeto());
                ps.setInt(6, codigo);
                ps.execute();
                System.out.println("Alterado com sucesso!");

            } catch (SQLException e) {
                System.out.println("Erro: " + e);
            }
        } else {
            System.out.println("Código não encontrado!");
        }
    }

    /*
    public void imprimirListaRelatoriosCadastradas() {

        try {
            ps = Persistencia.conexao().prepareStatement("select* from relatorios");
            rs = ps.executeQuery();
            System.out.println("_________________________________");
            System.out.println("|ID\t|Nome\t\t|CPF\t\t|E-mail\t\t\t|Sexo\t|Telefone");
            while (rs.next()) {
                int id = rs.getInt("idRelatorio");
                String nome = rs.getString("nome");
                String cpf = rs.getString("cpf");
                String email = rs.getString("email");
                String sexo = rs.getString("sexo");
                String telefone = rs.getString("telefone");

                System.out.println("|" + id + "\t|" + nome + "\t\t|" + cpf + "\t\t|" + email + "\t\t\t|" + sexo + "\t|" + telefone);
                System.out.println("");
            }
            rs.close();
        } catch (SQLException e) {
            System.out.println("Erro: " + e);
        }
    }
     */
    public boolean jaExiste(int codigo) {
        try {
            ps = Persistencia.conexao().prepareStatement("Select * from relatorios where codigoRelatorio=?");
            ps.setInt(1, codigo);
            rs = ps.executeQuery();
            if (rs.wasNull()) {
                return false;
            }
            rs.close();

        } catch (SQLException e) {
            System.out.println("Esse comando tá errado, pow!" + e);
        }
        return true;
    }

    /*
    public void alterarRelatorioNoBD(int codigo) {

        if (jaExiste(codigo)) {
            //r.preencher();
            try {
                ps = Persistencia.conexao().prepareStatement("Update relatorios set nome=?, cpf=?, email=?, sexo=?, telefone=?, rua=?, numero=?, complemento=?, bairro=?, cidade=?, uf=? where idRelatorio=?");
                ps.setString(1, r.getNome());
                ps.setString(2, r.getCpf());
                ps.setString(3, r.getEmail());
                ps.setString(4, r.getSexo());
                ps.setString(5, r.getTelefone());
                ps.setInt(6, codigo);
                ps.setString(7, r.getRua());
                ps.setInt(8, r.getNumero());
                ps.setString(9, r.getComplemento());
                ps.setString(10, r.getBairro());
                ps.setString(11, r.getCidade());
                ps.setString(12, r.getUf());
                ps.execute();
                System.out.println("Alterado com sucesso!");

            } catch (SQLException e) {
                System.out.println("Erro: " + e);
            }
        } else {
            System.out.println("Código não encontrado!");
        }
    }
     */
    public void remover(int codigo) {
        //if (jaExiste(codigo)) {
        try {
            ps = Persistencia.conexao().prepareStatement("Delete from relatorios where codigoRelatorio=?");
            ps.setInt(1, codigo);
            ps.executeUpdate();
            System.out.println("Relatorio excluído com sucesso!");

        } catch (SQLException e) {
            Logger.getLogger(Relatorio.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    /*} else {
            System.out.println("Código não encontrado!");
        }
    }*/
    /**
     * @return the ps
     */
    public PreparedStatement getPs() {
        return ps;
    }

    /**
     * @param ps the ps to set
     */
    public void setPs(PreparedStatement ps) {
        this.ps = ps;
    }

    /**
     * @return the rs
     */
    public ResultSet getRs() {
        return rs;
    }

    /**
     * @param rs the rs to set
     */
    public void setRs(ResultSet rs) {
        this.rs = rs;
    }

    /**
     * @return the r
     */
    public Relatorio getR() {
        return r;
    }

    /**
     * @param r the p to set
     */
    public void setR(Relatorio r) {
        this.r = r;
    }
}
