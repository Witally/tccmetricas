/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.Date;
import java.util.Scanner;

public class Projeto {

    private String nomeProjeto;
    private Date dataProjeto;
    private String descricaoProjeto;
    private String tipoProjeto;
    private String gerenteProjeto;
    private int codigoProjeto;

    public Projeto() {
        nomeProjeto = "";
        dataProjeto = null;
        descricaoProjeto = "";
        tipoProjeto = "";
        gerenteProjeto = "";
        codigoProjeto = 0;

    }

    public Projeto(int codigoProjeto, String nomeProjeto, Date dataProjeto, String tipoProjeto, String gerenteProjeto, String descricaoProjeto) {

        this.codigoProjeto = codigoProjeto;

        this.nomeProjeto = nomeProjeto;

        this.dataProjeto = dataProjeto;

        this.tipoProjeto = tipoProjeto;

        this.gerenteProjeto = gerenteProjeto;

        this.descricaoProjeto = descricaoProjeto;

    }

    /**
     * @return the descricaoProjeto
     */
    public String getDescricaoProjeto() {
        return descricaoProjeto;
    }

    /**
     * @param descricaoProjeto the descricaoProjeto to set
     */
    public void setDescricaoProjeto(String descricaoProjeto) {
        this.descricaoProjeto = descricaoProjeto;
    }

    /**
     * @return the nomeProjeto
     */
    public String getNomeProjeto() {
        return nomeProjeto;
    }

    /**
     * @param nomeProjeto the nomeProjeto to set
     */
    public void setNomeProjeto(String nomeProjeto) {
        this.nomeProjeto = nomeProjeto;
    }

    /**
     * @return the dataProjeto
     */
    public Date getDataProjeto() {
        return dataProjeto;
    }

    /**
     * @param dataProjeto the dataProjeto to set
     */
    public void setDataProjeto(Date dataProjeto) {
        this.dataProjeto = dataProjeto;
    }

    /**
     * @return the codigoProjeto
     */
    public int getCodigoProjeto() {
        return codigoProjeto;
    }

    /**
     * @param codigoProjeto the codigoProjeto to set
     */
    public void setCodigoProjeto(int codigoProjeto) {
        this.codigoProjeto = codigoProjeto;
    }

    public String getTipoProjeto() {
        return tipoProjeto;
    }

    public void setTipoProjeto(String tipoProjeto) {

        this.tipoProjeto = tipoProjeto;
    }

    public String getGerenteProjeto() {
        return gerenteProjeto;
    }

    public void setGerenteProjeto(String gerenteProjeto) {
        this.gerenteProjeto = gerenteProjeto;
    }

}
