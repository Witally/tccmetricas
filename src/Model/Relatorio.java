/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.Date;
import java.util.Scanner;

/**
 *
 * @author PauloAfonso
 */
public class Relatorio {

    private String descricao;
    private String nomeProjeto;
    private String nomeMetrica;
    private Date dataRelatorio;
    private int codigoRelatorio;
    private int codigoProjeto;
    private int horas;

    public Relatorio() {
        nomeProjeto = "";
        dataRelatorio = null;
        nomeMetrica = "";
        horas = 0;
//codigoRelatorio = 0;

    }

    public Relatorio(String nomeProjeto, Date dataRelatorio, String nomeMetrica, int horas, int codigoRelatorio) {

        this.nomeProjeto = nomeProjeto;

        this.dataRelatorio = dataRelatorio;

        this.nomeMetrica = nomeMetrica;

        this.codigoRelatorio = codigoRelatorio;

        this.horas = horas;

    }

    /**
     * @return the nomeMetrica
     */
    public String getDescricaoRelatorio() {
        return getNomeMetrica();
    }

    /**
     * @param nomeMetrica the nomeMetrica to set
     */
    public void setDescricaoRelatorio(String nomeMetrica) {
        this.setNomeMetrica(nomeMetrica);
    }

    /**
     * @return the nomeProjeto
     */
    public String getNomeRelatorio() {
        return getNomeProjeto();
    }

    /**
     * @param nomeProjeto the nomeProjeto to set
     */
    public void setNomeRelatorio(String nomeProjeto) {
        this.setNomeProjeto(nomeProjeto);
    }

    /**
     * @return the dataRelatorio
     */
    public Date getDataRelatorio() {
        return dataRelatorio;
    }

    /**
     * @param dataRelatorio the dataRelatorio to set
     */
    public void setDataRelatorio(Date dataRelatorio) {
        this.dataRelatorio = dataRelatorio;
    }

    /**
     * @return the codigoRelatorio
     */
    public int getCodigoRelatorio() {
        return codigoRelatorio;
    }

    /**
     * @param codigoRelatorio the codigoRelatorio to set
     */
    public void setCodigoRelatorio(int codigoRelatorio) {
        this.codigoRelatorio = codigoRelatorio;
    }

    /**
     * @return the nomeProjeto
     */
    public String getNomeProjeto() {
        return nomeProjeto;
    }

    /**
     * @param nomeProjeto the nomeProjeto to set
     */
    public void setNomeProjeto(String nomeProjeto) {
        this.nomeProjeto = nomeProjeto;
    }

    /**
     * @return the nomeMetrica
     */
    public String getNomeMetrica() {
        return nomeMetrica;
    }

    /**
     * @param nomeMetrica the nomeMetrica to set
     */
    public void setNomeMetrica(String nomeMetrica) {
        this.nomeMetrica = nomeMetrica;
    }

    /**
     * @return the horas
     */
    public int getHoras() {
        return horas;
    }

    /**
     * @param horas the horas to set
     */
    public void setHoras(int horas) {
        this.horas = horas;
    }

    /**
     * @return the descricao
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * @param descricao the descricao to set
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    /**
     * @return the codigoProjeto
     */
    public int getCodigoProjeto() {
        return codigoProjeto;
    }

    /**
     * @param codigoProjeto the codigoProjeto to set
     */
    public void setCodigoProjeto(int codigoProjeto) {
        this.codigoProjeto = codigoProjeto;
    }

}
