/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import java.net.URL;

import java.util.Optional;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import Model.Relatorio;
import Model.RelatorioBD;

/**
 *
 * @author PauloAfonso
 */
public class RelatoriosController implements Initializable {

    private Relatorio p = new Relatorio();

    private int linha;

    private RelatorioBD projetoBD = new RelatorioBD();

    private Stage stage;

    @FXML
    private TableView tabela;

    @FXML
    private TableColumn colunaProjeto;

    @FXML
    private TableColumn colunaData;

    @FXML
    private TableColumn colunaCodigoProjeto;

    @FXML
    private TableColumn colunaHoras;

    @FXML
    private TableColumn colunaMetrica;

    @FXML
    private TableColumn colunaCodigo;

    private ObservableList<Relatorio> lista;

    @FXML
    private Button botaoVisualizar;

    @FXML
    private Button botaoRemover;

    @FXML
    private Button botaoComparar;

    @FXML
    private Button botaoProjetos;

    @FXML
    private Button botaoInicio;

    @FXML
    private Button botaoSair;

    @FXML
    private AnchorPane conteudoRelatorios;

    @FXML
    private void botaoSair(ActionEvent event) {
        Alert alert = new Alert(AlertType.CONFIRMATION);

        alert.setTitle("SAIR");
        alert.setHeaderText("Você tem certeza que deseja sair?");
        alert.setContentText("Todos os dados que não foram salvos serão perdidos.");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK) {
            Platform.exit();
        } else {
        }
    }

    @FXML
    private void botaoMetricas(ActionEvent event) {
        getConteudoRelatorios().getChildren().clear();
        getConteudoRelatorios().getChildren().add(getNode("/View1/Metricas.fxml"));
    }

    @FXML
    private void botaoInicio(ActionEvent event) {
        getConteudoRelatorios().getChildren().clear();
        getConteudoRelatorios().getChildren().add(getNode("/View1/Menu.fxml"));
    }

    @FXML
    private void botaoProjetos(ActionEvent event) {
        getConteudoRelatorios().getChildren().clear();
        getConteudoRelatorios().getChildren().add(getNode("/View1/Projetos.fxml"));
    }

    @FXML
    private void botaoRelatorios(ActionEvent event) {
        getConteudoRelatorios().getChildren().clear();
        getConteudoRelatorios().getChildren().add(getNode("/View1/Relatorios.fxml"));
    }

    @FXML
    private void botaoCreditos(ActionEvent event) {
        getConteudoRelatorios().getChildren().clear();
        getConteudoRelatorios().getChildren().add(getNode("/View1/Creditos.fxml"));
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        mostrarTabela();
    }

    public Node getNode(String node) {
        Node no = null;
        try {
            no = FXMLLoader.load(getClass().getResource(node));

        } catch (Exception e) {
        }
        return no;

    }

    /**
     * @return the stage
     */
    public Stage getStage() {
        return stage;
    }

    /**
     * @param stage the stage to set
     */
    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    private void botaoVisualizar(ActionEvent event) {
        //getConteudoRelatorios().getChildren().clear();
        //getConteudoRelatorios().getChildren().add(getNode("/View2/CRUD/Cadastrar.fxml"));
    }

    @FXML
    private void botaoRemover(ActionEvent event) {

        int linha = getTabela().getSelectionModel().getSelectedIndex();
        int codigo = (int) getColunaCodigo().getCellData(linha);
        getRelatorioBD().remover(codigo);
        getConteudoRelatorios().getChildren().add(getNode("/View1/Relatorios.fxml"));
//mostrarTabela();
    }

    @FXML
    private void botaoComparar(ActionEvent event) {
        //getConteudoRelatorios().getChildren().clear();
        //conteudoRelatorios.getChildren().add(getNode("/View2/CRUD/Comparar.fxml"));
    }

    private void mostrarTabela() {
        setLista(getRelatorioBD().listaRelatoriosCadastrados());

        getTabela().setItems(getLista());

        //TableColumn colunaProjetos = new TableColumn("Nome");
        getColunaCodigo().setCellValueFactory(new PropertyValueFactory("codigoRelatorio"));

        getColunaProjeto().setCellValueFactory(new PropertyValueFactory("nomeProjeto"));

        getColunaCodigoProjeto().setCellValueFactory(new PropertyValueFactory("codigoProjeto"));

        getColunaMetrica().setCellValueFactory(new PropertyValueFactory("nomeMetrica"));

        getColunaData().setCellValueFactory(new PropertyValueFactory("dataRelatorio"));

        getColunaHoras().setCellValueFactory(new PropertyValueFactory("horas"));

        tabela.getSelectionModel().select(0);

        //   tabela.getColumns().addAll(colunaCodigo, colunaProjeto, colunaData, colunaDescricao);
    }

    /**
     * @return the p
     */
    public Relatorio getP() {
        return p;
    }

    /**
     * @param p the p to set
     */
    public void setP(Relatorio p) {
        this.p = p;
    }

    /**
     * @return the projetoBD
     */
    public RelatorioBD getRelatorioBD() {
        return getProjetoBD();
    }

    /**
     * @param projetoBD the projetoBD to set
     */
    public void setRelatorioBD(RelatorioBD projetoBD) {
        this.setProjetoBD(projetoBD);
    }

    /**
     * @return the tabela
     */
    public TableView getTabela() {
        return tabela;
    }

    /**
     * @param tabela the tabela to set
     */
    public void setTabela(TableView tabela) {
        this.tabela = tabela;
    }

    /**
     * @return the colunaProjeto
     */
    public TableColumn getColunaProjeto() {
        return colunaProjeto;
    }

    /**
     * @param colunaProjeto the colunaProjeto to set
     */
    public void setColunaProjeto(TableColumn colunaProjeto) {
        this.colunaProjeto = colunaProjeto;
    }

    /**
     * @return the colunaData
     */
    public TableColumn getColunaData() {
        return colunaData;
    }

    /**
     * @param colunaData the colunaData to set
     */
    public void setColunaData(TableColumn colunaData) {
        this.colunaData = colunaData;
    }

    /**
     * @return the colunaCodigo
     */
    public TableColumn getColunaCodigo() {
        return colunaCodigo;
    }

    /**
     * @param colunaCodigo the colunaCodigo to set
     */
    public void setColunaCodigo(TableColumn colunaCodigo) {
        this.colunaCodigo = colunaCodigo;
    }

    /**
     * @return the lista
     */
    public ObservableList<Relatorio> getLista() {
        return lista;
    }

    /**
     * @param lista the lista to set
     */
    public void setLista(ObservableList<Relatorio> lista) {
        this.lista = lista;
    }

    /**
     * @return the botaoRemover
     */
    public Button getBotaoRemover() {
        return botaoRemover;
    }

    /**
     * @param botaoRemover the botaoRemover to set
     */
    public void setBotaoRemover(Button botaoRemover) {
        this.botaoRemover = botaoRemover;
    }

    /**
     * @return the botaoComparar
     */
    public Button getBotaoComparar() {
        return botaoComparar;
    }

    /**
     * @param botaoComparar the botaoComparar to set
     */
    public void setBotaoComparar(Button botaoComparar) {
        this.botaoComparar = botaoComparar;
    }

    /**
     * @return the botaoProjetos
     */
    public Button getBotaoProjetos() {
        return botaoProjetos;
    }

    /**
     * @param botaoProjetos the botaoProjetos to set
     */
    public void setBotaoProjetos(Button botaoProjetos) {
        this.botaoProjetos = botaoProjetos;
    }

    /**
     * @return the botaoInicio
     */
    public Button getBotaoInicio() {
        return botaoInicio;
    }

    /**
     * @param botaoInicio the botaoInicio to set
     */
    public void setBotaoInicio(Button botaoInicio) {
        this.botaoInicio = botaoInicio;
    }

    /**
     * @return the botaoSair
     */
    public Button getBotaoSair() {
        return botaoSair;
    }

    /**
     * @param botaoSair the botaoSair to set
     */
    public void setBotaoSair(Button botaoSair) {
        this.botaoSair = botaoSair;
    }

    /**
     * @return the conteudoRelatorios
     */
    public AnchorPane getConteudoRelatorios() {
        return conteudoRelatorios;
    }

    /**
     * @param conteudoRelatorios the conteudoRelatorios to set
     */
    public void setConteudoRelatorios(AnchorPane conteudoRelatorios) {
        this.conteudoRelatorios = conteudoRelatorios;
    }

    /**
     * @return the linha
     */
    public int getLinha() {
        return linha;
    }

    /**
     * @param linha the linha to set
     */
    public void setLinha(int linha) {
        this.linha = linha;
    }

    /**
     * @return the colunaMetrica
     */
    public TableColumn getColunaMetrica() {
        return colunaMetrica;
    }

    /**
     * @param colunaMetrica the colunaMetrica to set
     */
    public void setColunaMetrica(TableColumn colunaMetrica) {
        this.colunaMetrica = colunaMetrica;
    }

    /**
     * @return the colunaHoras
     */
    public TableColumn getColunaHoras() {
        return colunaHoras;
    }

    /**
     * @param colunaHoras the colunaHoras to set
     */
    public void setColunaHoras(TableColumn colunaHoras) {
        this.colunaHoras = colunaHoras;
    }

    /**
     * @return the projetoBD
     */
    public RelatorioBD getProjetoBD() {
        return projetoBD;
    }

    /**
     * @param projetoBD the projetoBD to set
     */
    public void setProjetoBD(RelatorioBD projetoBD) {
        this.projetoBD = projetoBD;
    }

    /**
     * @return the colunaCodigoProjeto
     */
    public TableColumn getColunaCodigoProjeto() {
        return colunaCodigoProjeto;
    }

    /**
     * @param colunaCodigoProjeto the colunaCodigoProjeto to set
     */
    public void setColunaCodigoProjeto(TableColumn colunaCodigoProjeto) {
        this.colunaCodigoProjeto = colunaCodigoProjeto;
    }

    /**
     * @return the botaoVisualizar
     */
    public Button getBotaoVisualizar() {
        return botaoVisualizar;
    }

    /**
     * @param botaoVisualizar the botaoVisualizar to set
     */
    public void setBotaoVisualizar(Button botaoVisualizar) {
        this.botaoVisualizar = botaoVisualizar;
    }

}
