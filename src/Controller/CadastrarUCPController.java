/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import java.net.URL;
import java.util.ArrayList;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import Model.Relatorio;
import Model.RelatorioBD;

/**
 * FXML Controller class
 *
 * @author WIT
 */
public class CadastrarUCPController implements Initializable {

    private Relatorio r;

    public int cont = 0;

    private Stage stage;

    double horaProjeto = 0;

    @FXML
    Button botaoCalcular;

    @FXML
    Button botaoProjetos;

    @FXML
    Button botaoInicio;

    @FXML
    Pane pane1;

    @FXML
    Pane pane2;

    @FXML
    Pane pane3;

    @FXML
    Pane pane4;

    @FXML
    Pane pane5;

    @FXML
    Pane pane6;

    @FXML
    Pane pane7;

    @FXML
    Pane pane8;

    @FXML
    Pane pane9;

    @FXML
    TextField fatorT1;

    @FXML
    TextField fatorT2;

    @FXML
    TextField fatorT3;

    @FXML
    TextField fatorT4;

    @FXML
    TextField fatorT5;

    @FXML
    TextField fatorT6;

    @FXML
    TextField fatorT7;

    @FXML
    TextField fatorT8;

    @FXML
    TextField fatorT9;

    @FXML
    TextField fatorT10;

    @FXML
    TextField fatorT11;

    @FXML
    TextField fatorT12;

    @FXML
    TextField fatorT13;

    @FXML
    TextField fatorA1;

    @FXML
    TextField fatorA2;

    @FXML
    TextField fatorA3;

    @FXML
    TextField fatorA4;

    @FXML
    TextField fatorA5;

    @FXML
    TextField fatorA6;

    @FXML
    TextField fatorA7;

    @FXML
    TextField fatorA8;

    @FXML
    TextField atorSimples;

    @FXML
    TextField atorMedio;

    @FXML
    TextField atorComplexo;

    @FXML
    TextField cduSimples;

    @FXML
    TextField cduMedio;

    @FXML
    TextField cduComplexo;

    @FXML
    TextField textHoraSemanal;

    @FXML
    TextField textQtdPessoas;

    ArrayList<Pane> pane = new ArrayList<>();

    @FXML
    Button botaoRelatorios;

    @FXML
    Button botaoSair;
    @FXML
    Button botaoCancelar;
    @FXML
    Button botaoAplicar;

    @FXML
    Label labelUnidadeProjeto;

    @FXML
    Label labelUnidadePessoa;

    @FXML
    Label labelUnidade;

    @FXML
    AnchorPane conteudoCadastrarUCP;

    @FXML
    ComboBox Unidade;

    @FXML
    private void botaoSair(ActionEvent event) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);

        alert.setTitle("SAIR");
        alert.setHeaderText("Você tem certeza que deseja sair?");
        alert.setContentText("Todos os dados que não foram salvos serão perdidos.");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK) {
            Platform.exit();
        } else {
        }
    }

    @FXML
    private void botaoProjetos(ActionEvent event) {
        conteudoCadastrarUCP.getChildren().clear();
        conteudoCadastrarUCP.getChildren().add(getNode("/View1/Projetos.fxml"));
    }

    @FXML
    private void botaoMetricas(ActionEvent event) {
        conteudoCadastrarUCP.getChildren().clear();
        conteudoCadastrarUCP.getChildren().add(getNode("/View1/Metricas.fxml"));
    }

    @FXML
    private void botaoInicio(ActionEvent event) {
        conteudoCadastrarUCP.getChildren().clear();
        conteudoCadastrarUCP.getChildren().add(getNode("/View1/Menu.fxml"));
    }

    @FXML
    private void botaoRelatorios(ActionEvent event) {
        conteudoCadastrarUCP.getChildren().clear();
        conteudoCadastrarUCP.getChildren().add(getNode("/View1/Relatorios.fxml"));
    }

    @FXML
    private void botaoCreditos(ActionEvent event) {
        conteudoCadastrarUCP.getChildren().clear();
        conteudoCadastrarUCP.getChildren().add(getNode("/View1/Creditos.fxml"));
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        preencherComboBox();
        pane.add(pane1);
        pane.add(pane2);
        pane.add(pane3);
        pane.add(pane4);
        pane.add(pane5);
        pane.add(pane6);
        pane.add(pane7);
        pane.add(pane8);
        pane.add(pane9);

        mostrarTabela(cont);
    }

    public Node getNode(String node) {
        Node no = null;
        try {
            no = FXMLLoader.load(getClass().getResource(node));
        } catch (Exception e) {
        }
        return no;

    }

    @FXML
    private void botaoAplicar(ActionEvent event) {
        if (!botaoAplicar.getText().equals("CONCLUIR")) {
            cont++;
            mostrarTabela(cont);
        } else {

            RelatorioBD RelatorioBD = new RelatorioBD();
            Relatorio r = new Relatorio();

            conteudoCadastrarUCP.getChildren().clear();

            r.setNomeMetrica("UCP");
            r.setHoras((int) horaProjeto);

            //System.out.println((Date) c.getTime());
            //r.setDataRelatorio((Date) c.getTime());
            RelatorioBD.setR(r);
            RelatorioBD.gravarRelatorioNoBD(r);

            conteudoCadastrarUCP.getChildren().add(getNode("/View1/Relatorios.fxml"));
        }
    }

    @FXML
    private void botaoCancelar(ActionEvent event) {
        cont--;
        mostrarTabela(cont);
    }

    private void preencherComboBox() {
        ObservableList<String> options = FXCollections.observableArrayList(
                "HORAS",
                "SEMANAS",
                "MESES"
        );
        Unidade.setItems(options);
    }

    @FXML
    private void botaoCalcular(ActionEvent event) {

        horaProjeto = Double.parseDouble(textHoraSemanal.getText()) * calcularUCP();

        double horaPessoa = horaProjeto / Double.parseDouble(textQtdPessoas.getText());

        System.out.println(Unidade.getValue());
        if (Unidade.getValue() == null) {
            Unidade.setValue("HORAS");
        }

        if (Unidade.getValue().equals("HORAS")) {

        } else if (Unidade.getValue().equals("SEMANAS")) {
            horaProjeto /= Double.parseDouble(textHoraSemanal.getText());
            horaPessoa = horaProjeto / Double.parseDouble(textQtdPessoas.getText());
        } else if (Unidade.getValue().equals("MESES")) {
            horaProjeto /= (Double.parseDouble(textHoraSemanal.getText()) * 4);
            horaPessoa = horaProjeto / Double.parseDouble(textQtdPessoas.getText());
        }
        labelUnidade.setText(Unidade.getValue().toString());

        try {
            labelUnidadeProjeto.setText(Integer.toString((int) horaProjeto) + " " + labelUnidade.getText());
        } catch (Exception e) {
        }

        try {
            labelUnidadePessoa.setText(Integer.toString((int) horaPessoa) + " " + labelUnidade.getText());
        } catch (Exception e) {
        }

    }

    public void enableOne(Pane p) {

        for (Pane pan : pane) {
            pan.setVisible(false);
            pan.setDisable(true);
        }

        if (cont == 0) {
            botaoCancelar.setDisable(true);
        } else {
            botaoCancelar.setDisable(false);
        }

        if (cont == pane.size() - 1) {
            botaoAplicar.setText("CONCLUIR");
        } else {
            botaoAplicar.setText("AVANÇAR");
        }
        p.setVisible(true);
        p.setDisable(false);
    }

    public void mostrarTabela(int cont) {

        enableOne(pane.get(cont));
    }

    /**
     * @return the stage
     */
    public Stage getStage() {
        return stage;
    }

    /**
     * @param stage the stage to set
     */
    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public int calcularPesoAtores() {

        int pesoAtores, pesoAtor1, pesoAtor2, pesoAtor3;
        try {
            pesoAtor1 = Integer.parseInt(atorSimples.getText());
        } catch (Exception e) {
            pesoAtor1 = 0;
        }

        try {
            pesoAtor2 = Integer.parseInt(atorMedio.getText());
        } catch (Exception e) {
            pesoAtor2 = 0;
        }

        try {
            pesoAtor3 = Integer.parseInt(atorComplexo.getText());
        } catch (Exception e) {
            pesoAtor3 = 0;
        }
        pesoAtores = (pesoAtor1 * 1) + (pesoAtor2 * 2) + (pesoAtor3 * 3);
        return pesoAtores;
    }

    public int calcularPesoCDU() {
        int pesoCDU, pesoCDU1, pesoCDU2, pesoCDU3;

        try {
            pesoCDU1 = Integer.parseInt(cduSimples.getText());
        } catch (Exception e) {
            pesoCDU1 = 0;
        }

        try {
            pesoCDU2 = Integer.parseInt(cduMedio.getText());
        } catch (Exception e) {
            pesoCDU2 = 0;
        }

        try {
            pesoCDU3 = Integer.parseInt(cduComplexo.getText());
        } catch (Exception e) {
            pesoCDU3 = 0;
        }

        pesoCDU = (pesoCDU1 * 5) + (pesoCDU2 * 10) + (pesoCDU3 * 15);
        return pesoCDU;
    }

    public double calcularEF() {
        double EF = 0;

        try {
            EF += (Double.parseDouble(fatorA1.getText()) * 1.5);
        } catch (Exception e) {

        }
        try {
            EF += (Double.parseDouble(fatorA2.getText()) * -1);
        } catch (Exception e) {

        }
        try {
            EF += (Double.parseDouble(fatorA3.getText()) * 0.5);
        } catch (Exception e) {

        }
        try {
            EF += (Double.parseDouble(fatorA4.getText()) * 0.5);
        } catch (Exception e) {

        }
        try {
            EF += (Double.parseDouble(fatorA5.getText()) * 1);
        } catch (Exception e) {

        }
        try {
            EF += (Double.parseDouble(fatorA6.getText()) * 1);
        } catch (Exception e) {

        }
        try {
            EF += (Double.parseDouble(fatorA7.getText()) * -1);
        } catch (Exception e) {

        }
        try {
            EF += (Double.parseDouble(fatorA8.getText()) * 2);
        } catch (Exception e) {

        }
        EF = 1.4 + (-0.03 * EF);
        return EF;
    }

    public double calcularTCF() {
        double TCF = 0;

        try {
            TCF += (Double.parseDouble(fatorT1.getText()) * 2);
        } catch (Exception e) {

        }

        try {
            TCF += (Double.parseDouble(fatorT2.getText()) * 1);
        } catch (Exception e) {

        }

        try {
            TCF += (Double.parseDouble(fatorT3.getText()) * 1);
        } catch (Exception e) {

        }

        try {
            TCF += (Double.parseDouble(fatorT4.getText()) * 1);
        } catch (Exception e) {

        }

        try {
            TCF += (Double.parseDouble(fatorT5.getText()) * 1);
        } catch (Exception e) {

        }

        try {
            TCF += (Double.parseDouble(fatorT6.getText()) * 0.5);
        } catch (Exception e) {

        }

        try {
            TCF += (Double.parseDouble(fatorT7.getText()) * 0.5);
        } catch (Exception e) {

        }

        try {
            TCF += (Double.parseDouble(fatorT8.getText()) * 2);
        } catch (Exception e) {

        }

        try {
            TCF += (Double.parseDouble(fatorT9.getText()) * 1);
        } catch (Exception e) {

        }

        try {
            TCF += (Double.parseDouble(fatorT10.getText()) * 1);
        } catch (Exception e) {

        }

        try {
            TCF += (Double.parseDouble(fatorT11.getText()) * 1);
        } catch (Exception e) {

        }

        try {
            TCF += (Double.parseDouble(fatorT12.getText()) * 1);
        } catch (Exception e) {

        }

        try {
            TCF += (Double.parseDouble(fatorT13.getText()) * 1);
        } catch (Exception e) {

        }

        TCF = 0.6 + (0.01 * TCF);
        return TCF;
    }

    public int calcularUUCP() {
        int UUCP;
        UUCP = calcularPesoAtores() + calcularPesoCDU();
        return UUCP;
    }

    public double calcularUCP() {
        double UCP;
        UCP = calcularUUCP() + calcularTCF() + calcularEF();
        return UCP;

    }

}
