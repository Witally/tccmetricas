/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 *
 * @author PauloAfonso
 */
public class MetricasController implements Initializable {

    private Stage stage;

    @FXML
    Label labelContinuar;

    @FXML
    Button botaoProjetos;

    @FXML
    Button botaoInicio;

    @FXML
    Button botaoRelatorios;

    @FXML
    Button botaoSair;

    @FXML
    AnchorPane conteudoMetricas;

    @FXML
    Button botaoAPT;

    @FXML
    Button botaoTCP;

    @FXML
    Button botaoUCP;

    @FXML
    Button botaoAPF;

    @FXML
    Button botaoCOCOMO;

    @FXML
    Label titulo;

    @FXML
    Text texto;

    /*A métrica APT realiza medição de atividades de testes em um projeto e é considerada por alguns autores como a métrica mais utilizada. O funcionamento da métrica APT consiste na análise de três elementos básicos, o tamanho total do sistema a ser desenvolvido, o nível de produtividade e as características de qualidade a serem testadas. Os valores obtidos no primeiro e terceiro itens, multiplicado pela produtividade, gera a estimativa em horas. A APT é umas das métricas mais complexas existentes, devido à quantidade de dados a serem considerados e a complexidade deles.*/
    @FXML
    private void botaoAPT() {
        labelContinuar.setDisable(true);
        labelContinuar.setVisible(false);
        titulo.setText("ANÁLISE DE PONTOS DE TESTE (APT)");
        texto.setText("A métrica APT realiza medição de atividades de testes em um projeto e é considerada por alguns autores como a métrica mais utilizada. O funcionamento da métrica APT consiste na análise de três elementos básicos, o tamanho total do sistema a ser desenvolvido, o nível de produtividade e as características de qualidade a serem testadas. Os valores obtidos no primeiro e terceiro itens, multiplicado pela produtividade, gera a estimativa em horas. A APT é umas das métricas mais complexas existentes, devido à quantidade de dados a serem considerados e a complexidade deles.");
    }

    @FXML
    private void botaoTCP() {
        labelContinuar.setDisable(false);
        labelContinuar.setVisible(true);
        titulo.setText("ANÁLISE DE PONTOS POR CASOS DE TESTE (TCP)");
        texto.setText("Realiza estimativa precisa de projetos de testes. Principal objetivo: Determinar o nível de complexidade de cada ciclo de teste, gerando uma representacão do esforço envolvido no teste. A estimativa gerada pela métrica TCP é gerada em atividades de testes diferentes, geralmente projetos são a combinação de quatro modelos diferentes: Geração de Caso de Teste; Geração automatizada de scripts; Execução de Teste Manual; Execução Automática de Testes.");
    }

    @FXML
    private void botaoAPF() {
        labelContinuar.setVisible(false);
        labelContinuar.setVisible(true);
        titulo.setText("ANÁLISE DE PONTOS POR FUNÇÃO (APF)");
        texto.setText("");
    }

    @FXML
    private void botaoCOCOMO() {
        labelContinuar.setVisible(false);
        labelContinuar.setVisible(true);
        titulo.setText("COCOMO II");
        texto.setText("");
    }

    @FXML
    private void labelContinuar() {
        labelContinuar.setDisable(true);
        labelContinuar.setVisible(false);
        titulo.setText("ANÁLISE DE PONTOS POR CASOS DE TESTE (TCP)");
        texto.setText("A análise TCP consiste em um processo de sete etapas, deﬁnidas em: \n 1- Identiﬁcar os Casos de Uso; \n 2- Identiﬁcar Casos de Teste; \n 3- Determinar TCP para Geração de Caso de Teste; \n 4- Determinar TCP para Automação; \n 5- Determinar TCP para execução manual; \n 6- Determinar TCP para Execução Automatizada; \n 7- Determinar TCP total.");
    }

    @FXML
    private void botaoUCP() {
        labelContinuar.setDisable(true);
        labelContinuar.setVisible(false);
        titulo.setText("ANÁLISE DE PONTOS POR CASOS DE USO (UCP)");
        texto.setText("A métrica UCP realiza estimativas de esforço com base nos casos de uso do sistema. A análise consiste em deﬁnir um nível de prioridade aos atores do Diagrama de Casos de Uso, assim serão deﬁnidos pesos para cada prioridade. Depois serão avaliados os casos de usos do sistema, onde serão separados por complexidade de implementação e deﬁnidos pesos. Então, soma-se os pesos dos atores com os pesos dos casos de uso, e posteriormente descobre-se o fator de complexidade técnica. Os dados são inseridos em uma fórmula geral, onde a estimativa é feita.");
    }

    @FXML
    private void botaoSair(ActionEvent event) {
        Alert alert = new Alert(AlertType.CONFIRMATION);

        alert.setTitle("SAIR");
        alert.setHeaderText("Você tem certeza que deseja sair?");
        alert.setContentText("Todos os dados que não foram salvos serão perdidos.");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK) {
            Platform.exit();
        } else {
        }
    }

    @FXML
    private void botaoProjetos(ActionEvent event) {
        conteudoMetricas.getChildren().clear();
        conteudoMetricas.getChildren().add(getNode("/View1/Projetos.fxml"));
    }

    @FXML
    private void botaoMetricas(ActionEvent event) {
        conteudoMetricas.getChildren().clear();
        conteudoMetricas.getChildren().add(getNode("/View1/Metricas.fxml"));
    }

    @FXML
    private void botaoInicio(ActionEvent event) {
        conteudoMetricas.getChildren().clear();
        conteudoMetricas.getChildren().add(getNode("/View1/Menu.fxml"));
    }

    @FXML
    private void botaoRelatorios(ActionEvent event) {
        conteudoMetricas.getChildren().clear();
        conteudoMetricas.getChildren().add(getNode("/View1/Relatorios.fxml"));
    }

    @FXML
    private void botaoCreditos(ActionEvent event) {
        conteudoMetricas.getChildren().clear();
        conteudoMetricas.getChildren().add(getNode("/View1/Creditos.fxml"));
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    public Node getNode(String node) {
        Node no = null;
        try {
            no = FXMLLoader.load(getClass().getResource(node));
        } catch (Exception e) {
        }
        return no;

    }

    /**
     * @return the stage
     */
    public Stage getStage() {
        return stage;
    }

    /**
     * @param stage the stage to set
     */
    public void setStage(Stage stage) {
        this.stage = stage;
    }

}
