/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import java.net.URL;
import java.sql.Date;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import Model.Projeto;
import Model.ProjetoBD;

public class ProjetosController implements Initializable {

    @FXML
    RadioButton radioAPT;

    @FXML
    RadioButton radioTCP;

    @FXML
    RadioButton radioUCP;

    @FXML
    RadioButton radioAPF;

    @FXML
    RadioButton radioCOCOMO;

    @FXML
    TextField nomeProjeto2;

    @FXML
    TextField tipoProjeto2;

    @FXML
    TextField gerenteProjeto2;

    @FXML
    DatePicker dataProjeto2;

    @FXML
    TextArea descricaoProjeto2;

    @FXML
    Button botaoAplicar;

    @FXML
    Button botaoEditar2;

    @FXML
    Button botaoCancelar2;

    @FXML
    Pane paneEditar2;

    @FXML
    Pane main;

    private Projeto p = new Projeto();

    //private int linha;
    private ProjetoBD projetoBD = new ProjetoBD();

    private Stage stage;

    @FXML
    private TableView tabela;

    @FXML
    private TableColumn colunaProjeto;

    @FXML
    private TableColumn colunaData;

    @FXML
    private TableColumn colunaDescricao;

    @FXML
    private TableColumn colunaCodigo;

    @FXML
    private TableColumn colunaTipo;

    @FXML
    private TableColumn colunaGerente;

    private ObservableList<Projeto> lista;

    @FXML
    private Button botaoCriar;

    @FXML
    private Button botaoEditar;

    @FXML
    private Button botaoRemover;

    @FXML
    private Button botaoProjetos;

    @FXML
    private Button botaoInicio;

    @FXML
    private Button botaoRelatorios;

    @FXML
    private Button botaoSair;

    @FXML
    private AnchorPane conteudoProjetos;

    @FXML
    private void botaoSair(ActionEvent event) {
        Alert alert = new Alert(AlertType.CONFIRMATION);

        alert.setTitle("SAIR");
        alert.setHeaderText("Você tem certeza que deseja sair?");
        alert.setContentText("Todos os dados que não foram salvos serão perdidos.");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK) {
            Platform.exit();
        } else {
        }
    }

    @FXML
    private void botaoProjetos(ActionEvent event) {
        getConteudoProjetos().getChildren().clear();
        getConteudoProjetos().getChildren().add(getNode("/View1/Projetos.fxml"));
    }

    @FXML
    private void botaoMetricas(ActionEvent event) {
        getConteudoProjetos().getChildren().clear();
        getConteudoProjetos().getChildren().add(getNode("/View1/Metricas.fxml"));
    }

    @FXML
    private void botaoInicio(ActionEvent event) {
        getConteudoProjetos().getChildren().clear();
        getConteudoProjetos().getChildren().add(getNode("/View1/Menu.fxml"));
    }

    @FXML
    private void botaoRelatorios(ActionEvent event) {
        getConteudoProjetos().getChildren().clear();
        getConteudoProjetos().getChildren().add(getNode("/View1/Relatorios.fxml"));
    }

    @FXML
    private void botaoCreditos(ActionEvent event) {
        getConteudoProjetos().getChildren().clear();
        getConteudoProjetos().getChildren().add(getNode("/View1/Creditos.fxml"));
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        mostrarTabela();
    }

    public Node getNode(String node) {
        Node no = null;
        try {
            no = FXMLLoader.load(getClass().getResource(node));

        } catch (Exception e) {
        }
        return no;

    }

    /**
     * @return the stage
     */
    public Stage getStage() {
        return stage;
    }

    /**
     * @param stage the stage to set
     */
    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    private void botaoCriar(ActionEvent event) {
        getConteudoProjetos().getChildren().clear();
        getConteudoProjetos().getChildren().add(getNode("/View2/CRUD/Cadastrar.fxml"));
    }

    @FXML
    private void botaoAplicar(ActionEvent event) {
        /*
        ProjetoBD pBD = new ProjetoBD();
        int linha = getTabela().getSelectionModel().getSelectedIndex();

        int codigoProjeto = (int) getColunaCodigo().getCellData(linha);
        //System.out.println(codigoProjeto);
        String nomeProjeto = getColunaProjeto().getCellData(linha).toString();
        //System.out.println(nomeProjeto);
        Date dataProjeto = (Date) getColunaData().getCellData(linha);
        String descricaoProjeto = getColunaDescricao().getCellData(linha).toString();

        getP().setCodigoProjeto(codigoProjeto);
        getP().setNomeProjeto(nomeProjeto);
        getP().setDataProjeto(dataProjeto);
        getP().setDescricaoProjeto(descricaoProjeto);
        
        pBD.setP(getP());
        pBD.gravarProjetoNoRelatorio0();*/

        //getConteudoProjetos().getChildren().clear();
        if (radioTCP.isSelected()) {

            getConteudoProjetos().getChildren().add(getNode("/View2/CRUD/CadastrarTCP.fxml"));
        }
        if (radioUCP.isSelected()) {
            CadastrarUCPController UCPControl = new CadastrarUCPController();
            getConteudoProjetos().getChildren().add(getNode("/View2/CRUD/CadastrarUCP.fxml"));

        }
        if (radioAPF.isSelected()) {
            CadastrarAPFController APFControl = new CadastrarAPFController();
            getConteudoProjetos().getChildren().add(getNode("/View2/CRUD/CadastrarAPF.fxml"));
        }

        if (radioCOCOMO.isSelected()) {
            CadastrarCocomoController CocomoControl = new CadastrarCocomoController();
            getConteudoProjetos().getChildren().add(getNode("/View2/CRUD/CadastrarCocomo.fxml"));
        }
        if (radioAPT.isSelected()) {
            getConteudoProjetos().getChildren().add(getNode("/View2/CRUD/CadastrarAPT.fxml"));
        }

    }

    @FXML
    private void radioAPT() {
        if (radioAPT.isSelected()) {
            radioTCP.setSelected(false);
            radioUCP.setSelected(false);
            radioAPF.setSelected(false);
            radioCOCOMO.setSelected(false);
        } else {
            radioAPT.setSelected(true);
        }
    }

    @FXML
    private void radioTCP() {
        if (radioTCP.isSelected()) {
            radioAPT.setSelected(false);
            radioUCP.setSelected(false);
            radioAPF.setSelected(false);
            radioCOCOMO.setSelected(false);
        } else {
            radioTCP.setSelected(true);
        }
    }

    @FXML
    private void radioUCP() {
        if (radioUCP.isSelected()) {
            radioAPT.setSelected(false);
            radioTCP.setSelected(false);
            radioCOCOMO.setSelected(false);
            radioAPF.setSelected(false);
        } else {
            radioUCP.setSelected(true);
        }
    }

    @FXML
    private void radioCOCOMO() {
        if (radioCOCOMO.isSelected()) {
            radioUCP.setSelected(false);
            radioAPT.setSelected(false);
            radioTCP.setSelected(false);
            radioAPF.setSelected(false);
        } else {
            radioCOCOMO.setSelected(true);
        }
    }

    @FXML
    private void radioAPF() {
        if (radioAPF.isSelected()) {
            radioUCP.setSelected(false);
            radioAPT.setSelected(false);
            radioTCP.setSelected(false);
            radioCOCOMO.setSelected(false);

        } else {
            radioAPF.setSelected(true);
        }
    }

    @FXML
    private void botaoEditar(ActionEvent event) {
        //ProjetoBD ProjetoBD = new ProjetoBD();

        try {
            int linha = getTabela().getSelectionModel().getSelectedIndex();

            int codigoProjeto = (int) getColunaCodigo().getCellData(linha);
            System.out.println(codigoProjeto);

            String nomeProjeto = getColunaProjeto().getCellData(linha).toString();
            System.out.println(nomeProjeto);

            Date dataProjeto = (Date) getColunaData().getCellData(linha);

            String tipoProjeto = getColunaTipo().getCellData(linha).toString();
            System.out.println(tipoProjeto);

            String gerenteProjeto = getColunaGerente().getCellData(linha).toString();
            System.out.println(gerenteProjeto);

            String descricaoProjeto = getColunaDescricao().getCellData(linha).toString();

            getP().setCodigoProjeto(codigoProjeto);
            getP().setNomeProjeto(nomeProjeto);
            getP().setDataProjeto(dataProjeto);
            getP().setTipoProjeto(tipoProjeto);
            getP().setGerenteProjeto(gerenteProjeto);
            getP().setDescricaoProjeto(descricaoProjeto);

            main.setDisable(true);
            main.setVisible(false);

            paneEditar2.setDisable(false);
            paneEditar2.setVisible(true);

            nomeProjeto2.setText(p.getNomeProjeto());
            dataProjeto2.setValue(p.getDataProjeto().toLocalDate());
            tipoProjeto2.setText(p.getTipoProjeto());
            gerenteProjeto2.setText(p.getGerenteProjeto());
            descricaoProjeto2.setText(p.getDescricaoProjeto());

            //ProjetoBD.setP(p);
            //ProjetoBD.alterarProjetoNoBD(p.getCodigoProjeto());
            //paneEditar2.getChildren().add(getNode("/View1/Projetos.fxml"));
        } catch (Exception e) {
            System.out.println("Erro: " + e);
        }

    }

    @FXML
    private void botaoRemover(ActionEvent event) {

        int linha = getTabela().getSelectionModel().getSelectedIndex();
        int codigo = (int) getColunaCodigo().getCellData(linha);
        getProjetoBD().remover(codigo);
        getConteudoProjetos().getChildren().add(getNode("/View1/Projetos.fxml"));
//mostrarTabela();
    }

    private void mostrarTabela() {
        setLista(getProjetoBD().listaProjetosCadastrados());

        getTabela().setItems(getLista());

        //TableColumn colunaProjetos = new TableColumn("Nome");
        getColunaCodigo().setCellValueFactory(new PropertyValueFactory("codigoProjeto"));

        getColunaProjeto().setCellValueFactory(new PropertyValueFactory("nomeProjeto"));

        getColunaData().setCellValueFactory(new PropertyValueFactory("dataProjeto"));

        getColunaTipo().setCellValueFactory(new PropertyValueFactory("tipoProjeto"));

        getColunaGerente().setCellValueFactory(new PropertyValueFactory("gerenteProjeto"));

        getColunaDescricao().setCellValueFactory(new PropertyValueFactory("descricaoProjeto"));

        tabela.getSelectionModel().select(0);

        //   tabela.getColumns().addAll(colunaCodigo, colunaProjeto, colunaData, colunaDescricao);
    }

    /**
     * @return the p
     */
    public Projeto getP() {
        return p;
    }

    /**
     * @param p the p to set
     */
    public void setP(Projeto p) {
        this.p = p;
    }

    /**
     * @return the projetoBD
     */
    public ProjetoBD getProjetoBD() {
        return projetoBD;
    }

    /**
     * @param projetoBD the projetoBD to set
     */
    public void setProjetoBD(ProjetoBD projetoBD) {
        this.projetoBD = projetoBD;
    }

    /**
     * @return the tabela
     */
    public TableView getTabela() {
        return tabela;
    }

    /**
     * @param tabela the tabela to set
     */
    public void setTabela(TableView tabela) {
        this.tabela = tabela;
    }

    /**
     * @return the colunaProjeto
     */
    public TableColumn getColunaProjeto() {
        return colunaProjeto;
    }

    /**
     * @param colunaProjeto the colunaProjeto to set
     */
    public void setColunaProjeto(TableColumn colunaProjeto) {
        this.colunaProjeto = colunaProjeto;
    }

    /**
     * @return the colunaData
     */
    public TableColumn getColunaData() {
        return colunaData;
    }

    /**
     * @param colunaData the colunaData to set
     */
    public void setColunaData(TableColumn colunaData) {
        this.colunaData = colunaData;
    }

    /**
     * @return the colunaDescricao
     */
    public TableColumn getColunaDescricao() {
        return colunaDescricao;
    }

    /**
     * @param colunaDescricao the colunaDescricao to set
     */
    public void setColunaDescricao(TableColumn colunaDescricao) {
        this.colunaDescricao = colunaDescricao;
    }

    /**
     * @return the colunaCodigo
     */
    public TableColumn getColunaCodigo() {
        return colunaCodigo;
    }

    /**
     * @param colunaCodigo the colunaCodigo to set
     */
    public void setColunaCodigo(TableColumn colunaCodigo) {
        this.colunaCodigo = colunaCodigo;
    }

    /**
     * @return the lista
     */
    public ObservableList<Projeto> getLista() {
        return lista;
    }

    /**
     * @param lista the lista to set
     */
    public void setLista(ObservableList<Projeto> lista) {
        this.lista = lista;
    }

    /**
     * @return the botaoCriar
     */
    public Button getBotaoCriar() {
        return botaoCriar;
    }

    /**
     * @param botaoCriar the botaoCriar to set
     */
    public void setBotaoCriar(Button botaoCriar) {
        this.botaoCriar = botaoCriar;
    }

    /**
     * @return the botaoEditar
     */
    public Button getBotaoEditar() {
        return botaoEditar;
    }

    /**
     * @param botaoEditar the botaoEditar to set
     */
    public void setBotaoEditar(Button botaoEditar) {
        this.botaoEditar = botaoEditar;
    }

    /**
     * @return the botaoRemover
     */
    public Button getBotaoRemover() {
        return botaoRemover;
    }

    /**
     * @param botaoRemover the botaoRemover to set
     */
    public void setBotaoRemover(Button botaoRemover) {
        this.botaoRemover = botaoRemover;
    }

    /**
     * @return the botaoProjetos
     */
    public Button getBotaoProjetos() {
        return botaoProjetos;
    }

    /**
     * @param botaoProjetos the botaoProjetos to set
     */
    public void setBotaoProjetos(Button botaoProjetos) {
        this.botaoProjetos = botaoProjetos;
    }

    /**
     * @return the botaoInicio
     */
    public Button getBotaoInicio() {
        return botaoInicio;
    }

    /**
     * @param botaoInicio the botaoInicio to set
     */
    public void setBotaoInicio(Button botaoInicio) {
        this.botaoInicio = botaoInicio;
    }

    /**
     * @return the botaoRelatorios
     */
    public Button getBotaoRelatorios() {
        return botaoRelatorios;
    }

    /**
     * @param botaoRelatorios the botaoRelatorios to set
     */
    public void setBotaoRelatorios(Button botaoRelatorios) {
        this.botaoRelatorios = botaoRelatorios;
    }

    /**
     * @return the botaoSair
     */
    public Button getBotaoSair() {
        return botaoSair;
    }

    /**
     * @param botaoSair the botaoSair to set
     */
    public void setBotaoSair(Button botaoSair) {
        this.botaoSair = botaoSair;
    }

    /**
     * @return the conteudoProjetos
     */
    public AnchorPane getConteudoProjetos() {
        return conteudoProjetos;
    }

    /**
     * @param conteudoProjetos the conteudoProjetos to set
     */
    public void setConteudoProjetos(AnchorPane conteudoProjetos) {
        this.conteudoProjetos = conteudoProjetos;
    }

//EDITAR    
    @FXML
    private void botaoEditar2(ActionEvent event) {

        ProjetoBD ProjetoBD = new ProjetoBD();
        boolean certo = true;
        java.util.Date date = null;

        try {
            date = java.sql.Date.valueOf(dataProjeto2.getValue());
            p.setDataProjeto((Date) date);
            certo = true;
        } catch (Exception e) {
            certo = false;
        }

        if (certo) {

            paneEditar2.getChildren().clear();

            p.setNomeProjeto(nomeProjeto2.getText());
            p.setDataProjeto((Date) date);
            p.setTipoProjeto(tipoProjeto2.getText());
            p.setGerenteProjeto(gerenteProjeto2.getText());
            p.setDescricaoProjeto(descricaoProjeto2.getText());

            ProjetoBD.setP(p);
            ProjetoBD.alterarProjetoNoBD(p.getCodigoProjeto());

            paneEditar2.getChildren().add(getNode("/View1/Projetos.fxml"));
        } else {
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("INFO");
            alert.setHeaderText("CAMPO VAZIO!");
            alert.setContentText("Faltou preencher o campo DATA DO PROJETO.");
            alert.showAndWait();
        }
    }

    @FXML
    private void botaoCancelar2(ActionEvent event
    ) {
        paneEditar2.getChildren().clear();
        paneEditar2.getChildren().add(getNode("/View1/Projetos.fxml"));
    }

    public TableColumn getColunaTipo() {
        return colunaTipo;
    }

    public void setColunaTipo(TableColumn colunaTipo) {
        this.colunaTipo = colunaTipo;
    }

    public TableColumn getColunaGerente() {
        return colunaGerente;
    }

    public void setColunaGerente(TableColumn colunaGerente) {
        this.colunaGerente = colunaGerente;
    }

}
