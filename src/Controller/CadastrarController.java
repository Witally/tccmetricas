/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import java.net.URL;
import java.sql.Date;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import Model.Projeto;
import Model.ProjetoBD;

public class CadastrarController implements Initializable {

    @FXML
    TextField nomeProjeto;

    @FXML
    DatePicker dataProjeto;

    @FXML
    TextArea descricaoProjeto;

    @FXML
    TextField tipoProjeto;

    @FXML
    TextField gerenteProjeto;

    @FXML
    Button botaoCriar;

    @FXML
    Button botaoCancelar;

    private Stage stage;

    @FXML
    Button botaoProjetos;

    @FXML
    Button botaoInicio;

    @FXML
    Button botaoRelatorios;

    @FXML
    Button botaoSair;

    @FXML
    AnchorPane conteudoCadastrar;

    @FXML
    private void botaoSair(ActionEvent event) {
        Alert alert = new Alert(AlertType.CONFIRMATION);

        alert.setTitle("SAIR");
        alert.setHeaderText("Você tem certeza que deseja sair?");
        alert.setContentText("Todos os dados que não foram salvos serão perdidos.");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK) {
            Platform.exit();
        } else {
        }
    }

    @FXML
    private void botaoProjetos(ActionEvent event) {
        conteudoCadastrar.getChildren().clear();
        conteudoCadastrar.getChildren().add(getNode("/View1/Projetos.fxml"));
    }

    @FXML
    private void botaoMetricas(ActionEvent event) {
        conteudoCadastrar.getChildren().clear();
        conteudoCadastrar.getChildren().add(getNode("/View1/Metricas.fxml"));
    }

    @FXML
    private void botaoInicio(ActionEvent event) {
        conteudoCadastrar.getChildren().clear();
        conteudoCadastrar.getChildren().add(getNode("/View1/Menu.fxml"));
    }

    @FXML
    private void botaoRelatorios(ActionEvent event) {
        conteudoCadastrar.getChildren().clear();
        conteudoCadastrar.getChildren().add(getNode("/View1/Relatorios.fxml"));
    }

    @FXML
    private void botaoCreditos(ActionEvent event) {
        conteudoCadastrar.getChildren().clear();
        conteudoCadastrar.getChildren().add(getNode("/View1/Creditos.fxml"));
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    public Node getNode(String node) {
        Node no = null;
        try {
            no = FXMLLoader.load(getClass().getResource(node));
        } catch (Exception e) {
        }
        return no;

    }

    /**
     * @return the stage
     */
    public Stage getStage() {
        return stage;
    }

    /**
     * @param stage the stage to set
     */
    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    private void botaoCriar(ActionEvent event) {

        ProjetoBD ProjetoBD = new ProjetoBD();
        Projeto p = new Projeto();
        boolean certo;

        try {
            java.util.Date date = java.sql.Date.valueOf(dataProjeto.getValue());
            p.setDataProjeto((Date) date);
            certo = true;
        } catch (Exception e) {
            certo = false;
        }

        if (certo) {
            conteudoCadastrar.getChildren().clear();
            p.setNomeProjeto(nomeProjeto.getText());
            p.setTipoProjeto(tipoProjeto.getText());
            p.setGerenteProjeto(gerenteProjeto.getText());

            p.setDescricaoProjeto(descricaoProjeto.getText());
            ProjetoBD.setP(p);
            ProjetoBD.gravarProjetoNoBD(p);
            conteudoCadastrar.getChildren().add(getNode("/View1/Projetos.fxml"));
        } else {
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("INFO");
            alert.setHeaderText("CAMPO VAZIO!");
            alert.setContentText("Faltou preencher o campo DATA DO PROJETO.");
            alert.showAndWait();
        }
    }

    @FXML
    private void botaoCancelar(ActionEvent event
    ) {
        conteudoCadastrar.getChildren().clear();
        conteudoCadastrar.getChildren().add(getNode("/View1/Projetos.fxml"));
    }

}
