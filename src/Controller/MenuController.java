/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 *
 * @author PauloAfonso
 */
public class MenuController implements Initializable {

    private Stage stage;

    @FXML
    Button botaoProjetos;

    @FXML
    Button botaoContato;

    @FXML
    Button botaoRelatorios;

    @FXML
    Button botaoSair;

    @FXML
    AnchorPane conteudo;

    @FXML
    private void botaoSair(ActionEvent event) {
        Alert alert = new Alert(AlertType.CONFIRMATION);

        alert.setTitle("SAIR");
        alert.setHeaderText("Você tem certeza que deseja sair?");
        alert.setContentText("Todos os dados que não foram salvos serão perdidos.");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK) {
            Platform.exit();
        } else {
        }
    }

    @FXML
    private void botaoProjetos(ActionEvent event) {
        conteudo.getChildren().clear();
        conteudo.getChildren().add(getNode("/View1/Projetos.fxml"));
    }

    @FXML
    private void botaoMetricas(ActionEvent event) {
        conteudo.getChildren().clear();
        conteudo.getChildren().add(getNode("/View1/Metricas.fxml"));
    }

    @FXML
    private void botaoContato(ActionEvent event) {
        conteudo.getChildren().clear();
        conteudo.getChildren().add(getNode("/View1/Contato.fxml"));
    }

    @FXML
    private void botaoRelatorios(ActionEvent event) {
        conteudo.getChildren().clear();
        conteudo.getChildren().add(getNode("/View1/Relatorios.fxml"));
    }

    @FXML
    private void botaoCreditos(ActionEvent event) {
        conteudo.getChildren().clear();
        conteudo.getChildren().add(getNode("/View1/Creditos.fxml"));
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    public Node getNode(String node) {
        Node no = null;
        try {
            no = FXMLLoader.load(getClass().getResource(node));
        } catch (Exception e) {
        }
        return no;

    }

    /**
     * @return the stage
     */
    public Stage getStage() {
        return stage;
    }

    /**
     * @param stage the stage to set
     */
    public void setStage(Stage stage) {
        this.stage = stage;
    }

}
