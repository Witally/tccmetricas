/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import java.net.URL;
import java.util.ArrayList;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import Model.Relatorio;
import Model.RelatorioBD;

/**
 *
 * @author Witally
 */
public class CadastrarAPFController implements Initializable {

    private Relatorio r;

    public int cont = 0;

    private Stage stage;

    double horaProjeto = 0;

    @FXML
    Button botaoCalcular;

    @FXML
    Button botaoProjetos;

    @FXML
    Button botaoInicio;

    @FXML
    Pane pane1;

    @FXML
    Pane pane2;

    @FXML
    Pane pane3;

    @FXML
    Pane pane4;

    @FXML
    Pane pane5;

    @FXML
    Pane pane6;

    @FXML
    Pane pane7;

    @FXML
    Pane pane8;

    @FXML
    Pane pane9;

    @FXML
    Pane pane10;

    @FXML
    TextField fator1;

    @FXML
    TextField fator2;

    @FXML
    TextField fator3;

    @FXML
    TextField fator4;

    @FXML
    TextField fator5;

    @FXML
    TextField fator6;

    @FXML
    TextField fator7;

    @FXML
    TextField fator8;

    @FXML
    TextField fator9;

    @FXML
    TextField fator10;

    @FXML
    TextField fator11;

    @FXML
    TextField fator12;

    @FXML
    TextField fator13;

    @FXML
    TextField fator14;

    //campos ALI
    @FXML
    TextField baixaALI;
    @FXML
    TextField mediaALI;
    @FXML
    TextField altaALI;

    //CAMPOS AIE
    @FXML
    TextField baixaAIE;
    @FXML
    TextField mediaAIE;
    @FXML
    TextField altaAIE;

    //CAMPOS EE
    @FXML
    TextField baixaEE;
    @FXML
    TextField mediaEE;
    @FXML
    TextField altaEE;

    //CAMPOS SE
    @FXML
    TextField baixaSE;
    @FXML
    TextField mediaSE;
    @FXML
    TextField altaSE;

    //CAMPOS CE
    @FXML
    TextField baixaCE;
    @FXML
    TextField mediaCE;
    @FXML
    TextField altaCE;

    @FXML
    TextField textHoraSemanal;

    @FXML
    TextField textQtdPessoas;

    ArrayList<Pane> pane = new ArrayList<>();

    @FXML
    Button botaoRelatorios;

    @FXML
    Button botaoSair;
    @FXML
    Button botaoCancelar;
    @FXML
    Button botaoAplicar;

    @FXML
    Label labelUnidadeProjeto;

    @FXML
    Label labelUnidadePessoa;

    @FXML
    Label labelUnidade;

    @FXML
    AnchorPane conteudoCadastrarAPF;

    @FXML
    ComboBox Unidade;

    @FXML
    private void botaoSair(ActionEvent event) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);

        alert.setTitle("SAIR");
        alert.setHeaderText("Você tem certeza que deseja sair?");
        alert.setContentText("Todos os dados que não foram salvos serão perdidos.");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK) {
            Platform.exit();
        } else {
        }
    }

    @FXML
    private void botaoProjetos(ActionEvent event) {
        conteudoCadastrarAPF.getChildren().clear();
        conteudoCadastrarAPF.getChildren().add(getNode("/View1/Projetos.fxml"));
    }

    @FXML
    private void botaoMetricas(ActionEvent event) {
        conteudoCadastrarAPF.getChildren().clear();
        conteudoCadastrarAPF.getChildren().add(getNode("/View1/Metricas.fxml"));
    }

    @FXML
    private void botaoInicio(ActionEvent event) {
        conteudoCadastrarAPF.getChildren().clear();
        conteudoCadastrarAPF.getChildren().add(getNode("/View1/Menu.fxml"));
    }

    @FXML
    private void botaoRelatorios(ActionEvent event) {
        conteudoCadastrarAPF.getChildren().clear();
        conteudoCadastrarAPF.getChildren().add(getNode("/View1/Relatorios.fxml"));
    }

    @FXML
    private void botaoCreditos(ActionEvent event) {
        conteudoCadastrarAPF.getChildren().clear();
        conteudoCadastrarAPF.getChildren().add(getNode("/View1/Creditos.fxml"));
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        preencherComboBox();
        pane.add(pane1);
        pane.add(pane2);
        pane.add(pane3);
        pane.add(pane4);
        pane.add(pane5);
        pane.add(pane6);
        pane.add(pane7);
        pane.add(pane8);
        pane.add(pane9);
        pane.add(pane10);

        mostrarTabela(cont);
    }

    public Node getNode(String node) {
        Node no = null;
        try {
            no = FXMLLoader.load(getClass().getResource(node));
        } catch (Exception e) {
        }
        return no;

    }

    @FXML
    private void botaoAplicar(ActionEvent event) {
        if (!botaoAplicar.getText().equals("CONCLUIR")) {
            cont++;
            mostrarTabela(cont);
        } else {

            RelatorioBD RelatorioBD = new RelatorioBD();
            Relatorio r = new Relatorio();

            conteudoCadastrarAPF.getChildren().clear();

            r.setNomeMetrica("APF");
            r.setHoras((int) horaProjeto);

            //System.out.println((Date) c.getTime());
            //r.setDataRelatorio((Date) c.getTime());
            RelatorioBD.setR(r);
            RelatorioBD.gravarRelatorioNoBD(r);

            conteudoCadastrarAPF.getChildren().add(getNode("/View1/Relatorios.fxml"));
        }
    }

    @FXML
    private void botaoCancelar(ActionEvent event) {
        cont--;
        mostrarTabela(cont);
    }

    private void preencherComboBox() {
        ObservableList<String> options = FXCollections.observableArrayList(
                "HORAS",
                "SEMANAS",
                "MESES"
        );
        Unidade.setItems(options);
    }

    @FXML
    private void botaoCalcular(ActionEvent event) {

        horaProjeto = Double.parseDouble(textHoraSemanal.getText()) * calcularAPF();

        double horaPessoa = horaProjeto / Double.parseDouble(textQtdPessoas.getText());

        System.out.println(Unidade.getValue());
        if (Unidade.getValue() == null) {
            Unidade.setValue("HORAS");
        }

        if (Unidade.getValue().equals("HORAS")) {

        } else if (Unidade.getValue().equals("SEMANAS")) {
            horaProjeto /= Double.parseDouble(textHoraSemanal.getText());
            horaPessoa = horaProjeto / Double.parseDouble(textQtdPessoas.getText());
        } else if (Unidade.getValue().equals("MESES")) {
            horaProjeto /= (Double.parseDouble(textHoraSemanal.getText()) * 4);
            horaPessoa = horaProjeto / Double.parseDouble(textQtdPessoas.getText());
        }
        labelUnidade.setText(Unidade.getValue().toString());

        try {
            labelUnidadeProjeto.setText(Integer.toString((int) horaProjeto) + " " + labelUnidade.getText());
        } catch (Exception e) {
        }

        try {
            labelUnidadePessoa.setText(Integer.toString((int) horaPessoa) + " " + labelUnidade.getText());
        } catch (Exception e) {
        }

    }

    public void enableOne(Pane p) {

        for (Pane pan : pane) {
            pan.setVisible(false);
            pan.setDisable(true);
        }

        if (cont == 0) {
            botaoCancelar.setDisable(true);
        } else {
            botaoCancelar.setDisable(false);
        }

        if (cont == pane.size() - 1) {
            System.out.println(pane.size());
            botaoAplicar.setText("CONCLUIR");
        } else {

            botaoAplicar.setText("AVANÇAR");
        }
        p.setVisible(true);
        p.setDisable(false);
    }

    public void mostrarTabela(int cont) {

        enableOne(pane.get(cont));
    }

    /**
     * @return the stage
     */
    public Stage getStage() {
        return stage;
    }

    /**
     * @param stage the stage to set
     */
    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public int calcularPesoALI() {

        int pesoALI, bxoALI, mdoALI, altALI;
        try {
            bxoALI = Integer.parseInt(baixaALI.getText());
        } catch (Exception e) {
            bxoALI = 0;
        }

        try {
            mdoALI = Integer.parseInt(mediaALI.getText());
        } catch (Exception e) {
            mdoALI = 0;
        }

        try {
            altALI = Integer.parseInt(altaALI.getText());
        } catch (Exception e) {
            altALI = 0;
        }
        pesoALI = (bxoALI * 7) + (mdoALI * 10) + (altALI * 15);
        return pesoALI;
    }

    public int calcularPesoAIE() {
        int pesoAIE, pesoAIE1, pesoAIE2, pesoAIE3;

        try {
            pesoAIE1 = Integer.parseInt(baixaAIE.getText());
        } catch (Exception e) {
            pesoAIE1 = 0;
        }

        try {
            pesoAIE2 = Integer.parseInt(mediaAIE.getText());
        } catch (Exception e) {
            pesoAIE2 = 0;
        }

        try {
            pesoAIE3 = Integer.parseInt(altaAIE.getText());
        } catch (Exception e) {
            pesoAIE3 = 0;
        }

        pesoAIE = (pesoAIE1 * 5) + (pesoAIE2 * 7) + (pesoAIE3 * 10);
        return pesoAIE;
    }

    public int calcularPesoEE() {
        int pesoEE, pesoEE1, pesoEE2, pesoEE3;
        try {
            pesoEE1 = Integer.parseInt(baixaEE.getText());
        } catch (Exception e) {
            pesoEE1 = 0;
        }
        try {
            pesoEE2 = Integer.parseInt(mediaEE.getText());
        } catch (Exception e) {
            pesoEE2 = 0;
        }
        try {
            pesoEE3 = Integer.parseInt(altaEE.getText());
        } catch (Exception e) {
            pesoEE3 = 0;
        }
        pesoEE = (pesoEE1 * 3) + (pesoEE2 * 4) + (pesoEE3 * 6);
        return pesoEE;

    }

    public int calcularPesoSE() {
        int pesoSE, pesoSE1, pesoSE2, pesoSE3;
        try {
            pesoSE1 = Integer.parseInt(baixaSE.getText());
        } catch (Exception e) {
            pesoSE1 = 0;
        }
        try {
            pesoSE2 = Integer.parseInt(mediaSE.getText());
        } catch (Exception e) {
            pesoSE2 = 0;
        }
        try {
            pesoSE3 = Integer.parseInt(altaSE.getText());
        } catch (Exception e) {
            pesoSE3 = 0;
        }
        pesoSE = (pesoSE1 * 4) + (pesoSE2 * 5) + (pesoSE3 * 7);
        return pesoSE;

    }

    public int calcularPesoCE() {
        int pesoCE, pesoCE1, pesoCE2, pesoCE3;
        try {
            pesoCE1 = Integer.parseInt(baixaCE.getText());
        } catch (Exception e) {
            pesoCE1 = 0;
        }
        try {
            pesoCE2 = Integer.parseInt(mediaCE.getText());
        } catch (Exception e) {
            pesoCE2 = 0;
        }
        try {
            pesoCE3 = Integer.parseInt(altaCE.getText());
        } catch (Exception e) {
            pesoCE3 = 0;
        }
        pesoCE = (pesoCE1 * 3) + (pesoCE2 * 4) + (pesoCE3 * 6);
        return pesoCE;

    }

    public double calcularCG() {
        double CG = 0;

        try {
            CG += (Double.parseDouble(fator1.getText()));
        } catch (Exception e) {

        }

        try {
            CG += (Double.parseDouble(fator2.getText()));
        } catch (Exception e) {

        }

        try {
            CG += (Double.parseDouble(fator3.getText()));
        } catch (Exception e) {

        }

        try {
            CG += (Double.parseDouble(fator4.getText()));
        } catch (Exception e) {

        }

        try {
            CG += (Double.parseDouble(fator5.getText()));
        } catch (Exception e) {

        }

        try {
            CG += (Double.parseDouble(fator6.getText()));
        } catch (Exception e) {

        }

        try {
            CG += (Double.parseDouble(fator7.getText()));
        } catch (Exception e) {

        }

        try {
            CG += (Double.parseDouble(fator8.getText()));
        } catch (Exception e) {

        }

        try {
            CG += (Double.parseDouble(fator9.getText()));
        } catch (Exception e) {

        }

        try {
            CG += (Double.parseDouble(fator10.getText()));
        } catch (Exception e) {

        }

        try {
            CG += (Double.parseDouble(fator11.getText()));
        } catch (Exception e) {

        }

        try {
            CG += (Double.parseDouble(fator12.getText()));
        } catch (Exception e) {

        }

        try {
            CG += (Double.parseDouble(fator13.getText()));
        } catch (Exception e) {

        }

        try {
            CG += (Double.parseDouble(fator14.getText()));
        } catch (Exception e) {

        }

        return CG;
    }

    public double calcularFAV() {
        double FAV;
        FAV = 0.65 + ((calcularPesoALI() + calcularPesoAIE() + calcularPesoEE() + calcularPesoCE() + calcularPesoCE()) * 0.01);
        return FAV;
    }

    public double calcularAPF() {
        double APF;
        APF = calcularFAV() * calcularCG();
        return APF;

    }

}
